# Social Profile Discovery - serverless function

Goal of this task is creating serverless function that, given a public social media network user handle (person, company etc.), returns list of other known public social media network handles for such user.

## Expected Delivery

[OpenFaas](https://www.openfaas.com) serverless function (no specific template required, recommended Python3).

## Function I/O specs

Please refer to this project [OpenAPI json](./openapi.json) file.

## Toolkit

We'll provide access to dedicated GitLab repository.
We'll also provide dedicated OpenFaas gateway and token for deployment.

## Considerations

Commercial API endpoints (e.g. Google search API) can be leveraged. However for those we require cost evaluation for a 10k/day API calls scenario.
We are looking for maintanable, scalable and cost effective solution.

This function will be used on a variety of social profiles, including less popular social profiles, social profiles specific to a city (e.g. Hilton Hotel in New York City vs. Hilton Hotel in Los Angeles), and profiles with common names.  Consider the technical challenge of finding the correct social profiles for the same person/business/entity on other social media platforms.

E.g. Finding profiles for U2 the band:


    Input: {
        "handle": "u2",
        "platform": "facebook"
    }

    Output:
    [
        {
            "handle": "u2official",
            "platform": "youtube"
        },
        {
            "handle": "u2",
            "platform": "twitter"
        }
    ]



## Key Evaluation Criteria:
    - Code readability and documentation
    - Scalability and maintability
    - Cost effectiveness of solution
    - TDD is welcome
    - CI/CD practices
